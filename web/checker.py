from flask import session
from functools import wraps

# my own decorator


def check_logged_in(func):
    @wraps(func)
    def wrapper(*args, **kwordsargs):
        if 'logged_in' in session:
            return func(*args, **kwordsargs)
        return 'You are not logged in.'
    return wrapper
