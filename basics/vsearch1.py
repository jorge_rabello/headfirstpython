def search_for_vowels():
    """Display any vowels found in an asked-for word"""
    vowels = set('aeiou')
    word = input('Provide a word to search for vowels: ')
    found = vowels.intersection(set(word))
    for vowel in found:
        print(vowel)

search_for_vowels()
