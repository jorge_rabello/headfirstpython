def search_for_vowels(word):
    """Display any vowels found in an asked-for word"""
    vowels = set('aeiou')
    return vowels.intersection(set(word))

# word = input('Provide a word to search for vowels: ')
print(search_for_vowels(input('Provide a word to search for vowels: ')))
