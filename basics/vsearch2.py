def search_for_vowels(word):
    """Display any vowels found in an asked-for word"""
    vowels = set('aeiou')
    found = vowels.intersection(set(word))
    for vowel in found:
        print(vowel)


# word = input('Provide a word to search for vowels: ')
search_for_vowels(input('Provide a word to search for vowels: '))
