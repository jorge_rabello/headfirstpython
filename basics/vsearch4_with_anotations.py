# Anotations are optional and it is informational
# In this use case the first annotation states that the function expects a string (:str)
# and the second annotation states that the function returns a set to its caller (-> set):

def search_for_vowels(phrase:str) -> set:
    """Display any vowels found in a supplied phrase"""
    vowels = set('aeiou')
    return vowels.intersection(set(phrase))

# word = input('Provide a word to search for vowels: ')
print(search_for_vowels(input('Provide a phrase to search for vowels: ')))
